
var screen_h = window.innerHeight;

var chagneH_name = "body,.window_div,.menu-box,.box,.pagecover,.contentPage-1,.contentPage-2";
var changeTop_H_Name = ".menu-box,.contentPage-1,.contentPage-2";
var changeLineH_Name = ".pagecover,.logo-box";

$(window).on('load', function () {
    $(chagneH_name).height(screen_h);
    $(changeTop_H_Name).css("top",screen_h+"px");
    $(changeLineH_Name).css({"line-height":screen_h+"px"});
    $(".content-div").css({"height": (screen_h - 46) + "px"});
    setTimeout(function(){
        $(".fullpage").css({ "top": "-100%" });
    },2000)
});


$(window).resize(function(){
    screen_h = window.innerHeight;
    $(chagneH_name).height(screen_h);
    $(changeTop_H_Name).css("top",screen_h+"px");
    $(changeLineH_Name).css({"line-height":screen_h+"px"});
    $(".content-div").css({"height": (screen_h - 46) + "px"});
});

function move(page,_isopen){
    if(_isopen){
        $(page).css({ "left": "0%" });
    }else{
        $(page).css({ "left": "100%" });
    }
}

$(document).on('swiperight','.contentPage-1',function () {
    move(this,false);
})

$(document).on('swiperight','.contentPage-2', function () {
    move(this,false);
})

$(document).on('swiperight','.main-box',function () {
    // move(".menu-box",true);
    $(".fullpage").css({ "left": "100%" });
})
$(document).on('swipeleft','.menu-box', function () {
    // move(this,false);
    $(".fullpage").css({ "left": "0%" });
})
$(document).on('swipeup','.logo-box', function () {
    $(".fullpage").css({ "top": "-100%" });
})

$(document).on('click',"#content-1",function(){
    move(".contentPage-1",true);
})

$(document).on('click',"#content-2",function(){
    move(".contentPage-2",true);
})
$(document).on('click','.menuBtn', function () {
    $(".fullpage").css({ "left": "100%" });
})
$(document).on('click',".homeBtn",function(){
    move(".contentPage-1",false);
})
$(document).on('click',".angleBtn",function(){
    move(".contentPage-2",false);
})
